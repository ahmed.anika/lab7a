public class Card {
    private String suit;
    private int value;
    
    public Card (String suit, int value) {
        this.suit = suit;
        this.value = value;
    }

    public String getSuit() {
		return this.suit;
	}

    public int getValue() {
		return this.value;
	}
    public String toString() {
        String printValue = "";
        if ( value == 1) {
            printValue = "Ace";
        } else if ( value == 11) {
            printValue = "Jack";
        } else if ( value == 12) {
            printValue = "Queen";
        } else if ( value == 13) {
            printValue = "King";
        } else {
            printValue = Integer.toString (value);
        }
        return printValue + " of " + suit;
    }
	public Double calculateScore() {
		double score = 0.0;
		if ( suit.equals("Hearts")) {
			score = value + 0.4;
			return score;
		} else if (suit.equals("Spades")){
			score = value + 0.3;
			return score;
		} else if (suit.equals("Diamonds")){
			score = value + 0.2;
			return score;
		} else if (suit.equals("Clubs")){
			score = value + 0.1;
			return score;
		}
		return score;
	}


}