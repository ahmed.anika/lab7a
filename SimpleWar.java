public class SimpleWar {
	public static void main (String [] args){
		Deck deck = new Deck();
		deck.shuffle();
		//System.out.println(deck);
		int playerOnePoints = 0;
		int playerTwoPoints = 0;
		
		while ( deck.length() > 0 ){
			Card cardOne = deck.drawTopCard();
			Card cardTwo = deck.drawTopCard();
			System.out.println("Player has " + playerOnePoints + " point(s) & player 2 has " + playerTwoPoints + " point(s)" );
			System.out.println(cardOne);
			System.out.println(cardTwo);
			double playerOneScore = cardOne.calculateScore();
			double playerTwoScore = cardTwo.calculateScore();
			System.out.println(playerOneScore);
			System.out.println(playerTwoScore);
			
			if (playerOneScore > playerTwoScore){
				System.out.println("Player 1 wins!");
				playerOnePoints++;
			} else {
				System.out.println("Player 2 wins!");
				playerTwoPoints++;
			}
			
			System.out.println("Player has " + playerOnePoints + " point(s) & player 2 has " + playerTwoPoints + " point(s)" );
			System.out.println("*******************************************************************************");
		}
	
		if (playerOnePoints > playerTwoPoints){
			System.out.println("Congrats Player 1, you won!");
		
		} else {
			System.out.println("Congrats Player 2, you won!");
		
		}
		
		
	}
}